using System.Collections.Generic;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Security;

namespace Urs.Services.Security
{
    public partial class StandardPermissionProvider : IPermissionProvider
    {
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin area", SystemName = "AccessAdminPanel", Group = "Standard" };
        public static readonly PermissionRecord ManageBrands = new PermissionRecord { Name = "Admin area. Manage Brand", SystemName = "ManageBrand", Group = "Catalog" };
        public static readonly PermissionRecord ManageCatalog = new PermissionRecord { Name = "Admin area. Manage Catalog", SystemName = "ManageCatalog", Group = "Catalog" };
        public static readonly PermissionRecord ManageGoods = new PermissionRecord { Name = "Admin area. Manage Goods", SystemName = "ManageGoods", Group = "Catalog" };
        public static readonly PermissionRecord ManageGoodsReviews = new PermissionRecord { Name = "Admin area. Manage Goods Review", SystemName = "ManageGoodsReviews", Group = "Catalog" };
        public static readonly PermissionRecord ManageSpecAttributes = new PermissionRecord { Name = "Admin area. Manage Spec Attribute", SystemName = "ManageSpecAttributes", Group = "Catalog" };
        public static readonly PermissionRecord ManageUsers = new PermissionRecord { Name = "Admin area. Manage Users", SystemName = "ManageUsers", Group = "Users" };
        public static readonly PermissionRecord ManageUserRoles = new PermissionRecord { Name = "Admin area. Manage User Level", SystemName = "ManageUserRoles", Group = "Users" };
        public static readonly PermissionRecord ManageOrders = new PermissionRecord { Name = "Admin area. Manage Orders", SystemName = "ManageOrders", Group = "Orders" };
        public static readonly PermissionRecord ManageAfterSales = new PermissionRecord { Name = "Admin area. Manage Return Requests", SystemName = "ManageAfterSales", Group = "Orders" };
        public static readonly PermissionRecord ManageCoupons = new PermissionRecord { Name = "Admin area. Manage Coupon", SystemName = "ManageCoupons", Group = "Promo" };
        public static readonly PermissionRecord ManageWidgets = new PermissionRecord { Name = "Admin area. Manage Widgets", SystemName = "ManageWidgets", Group = "Content Management" };
        public static readonly PermissionRecord ManageTopics = new PermissionRecord { Name = "Admin area. Manage Topics", SystemName = "ManageTopics", Group = "Content Management" };
        public static readonly PermissionRecord ManageBanner = new PermissionRecord { Name = "Admin area. Manage Banner", SystemName = "ManageBanner", Group = "Content Management" };
        public static readonly PermissionRecord ManageLanguages = new PermissionRecord { Name = "Admin area. Manage Languages", SystemName = "ManageLanguages", Group = "Configuration" };
        public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin area. Manage Settings", SystemName = "ManageSettings", Group = "Configuration" };
        public static readonly PermissionRecord ManagePaymentMethods = new PermissionRecord { Name = "Admin area. Manage Payment Methods", SystemName = "ManagePaymentMethods", Group = "Configuration" };
        public static readonly PermissionRecord ManageExternalAuthenticationMethods = new PermissionRecord { Name = "Admin area. Manage External Authentication Methods", SystemName = "ManageExternalAuthenticationMethods", Group = "Configuration" };
        public static readonly PermissionRecord ManageShippingSettings = new PermissionRecord { Name = "Admin area. Manage Shipping Settings", SystemName = "ManageShippingSettings", Group = "Configuration" };
        public static readonly PermissionRecord ManageMeasures = new PermissionRecord { Name = "Admin area. Manage Measures", SystemName = "ManageMeasures", Group = "Configuration" };
        public static readonly PermissionRecord ManageActivityLog = new PermissionRecord { Name = "Admin area. Manage Activity Log", SystemName = "ManageActivityLog", Group = "Configuration" };
        public static readonly PermissionRecord ManageAcl = new PermissionRecord { Name = "Admin area. Manage ACL", SystemName = "ManageACL", Group = "Configuration" };
        public static readonly PermissionRecord ManagePlugins = new PermissionRecord { Name = "Admin area. Manage Plugins", SystemName = "ManagePlugins", Group = "Configuration" };
        public static readonly PermissionRecord ManageSystemLog = new PermissionRecord { Name = "Admin area. Manage System Log", SystemName = "ManageSystemLog", Group = "Configuration" };
        public static readonly PermissionRecord ManageScheduleTasks = new PermissionRecord { Name = "Admin area. Manage Schedule Tasks", SystemName = "ManageScheduleTasks", Group = "Configuration" };
        public static readonly PermissionRecord ManageFavorites = new PermissionRecord { Name = "Admin area.Manage Favorites", SystemName = "ManageFavorites", Group = "Configuration" };
        public static readonly PermissionRecord ManageAgents = new PermissionRecord { Name = "Admin area.Manage Users", SystemName = "ManageAgents", Group = "Customers" };


        public virtual IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[] 
            {
                AccessAdminPanel,
                ManageCatalog,
                ManageBanner,
                ManageUsers,
                ManageUserRoles,
                ManageOrders,
                ManageAfterSales,
                ManageWidgets,
                ManageTopics,
                ManageLanguages,
                ManageSettings,
                ManagePaymentMethods,
                ManageExternalAuthenticationMethods,
                ManageShippingSettings,
                ManageMeasures,
                ManageActivityLog,
                ManageAcl,
                ManagePlugins,
                ManageSystemLog,
                ManageScheduleTasks,
                ManageAgents
            };
        }

        public virtual IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[] 
            {
                new DefaultPermissionRecord 
                {
                    UserRoleSystemName = SystemRoleNames.Administrators,
                    PermissionRecords = new[] 
                    {
                        AccessAdminPanel,
                        ManageCatalog,
                        ManageBanner,
                        ManageUsers,
                        ManageUserRoles,
                        ManageOrders,
                        ManageAfterSales,
                        ManageWidgets,
                        ManageTopics,
                        ManageLanguages,
                        ManageSettings,
                        ManagePaymentMethods,
                        ManageExternalAuthenticationMethods,
                        ManageShippingSettings,
                        ManageMeasures,
                        ManageActivityLog,
                        ManageAcl,
                        ManagePlugins,
                        ManageSystemLog,
                        ManageScheduleTasks,
                        ManageAgents
                    }
                },
                new DefaultPermissionRecord 
                {
                    UserRoleSystemName = SystemRoleNames.Guests,
                },
                new DefaultPermissionRecord 
                {
                    UserRoleSystemName = SystemRoleNames.Registered,
                },
            };
        }
    }
}