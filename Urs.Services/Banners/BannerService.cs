using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Core;
using Urs.Core.Caching;
using Urs.Core.Data;
using Urs.Data.Domain.Banners;

namespace Urs.Services.Banners
{
    public partial class BannerService : IBannerService
    {
        #region Constants
        private const string BANNERSLIDER_ALL_KEY = "Urs.bannerslider.all-{0}-{1}";
        private const string BANNERSLIDER_PATTERN_KEY = "Urs.bannerslider.";
        #endregion

        #region Fields

        private readonly IRepository<Banner> _bannerRepository;
        private readonly IRepository<BannerItem> _bannerItemRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public BannerService(ICacheManager cacheManager,
            IRepository<Banner> sbwRepository,
            IRepository<BannerItem> sbwrdcRepository)
        {
            this._cacheManager = cacheManager;
            this._bannerRepository = sbwRepository;
            this._bannerItemRepository = sbwrdcRepository;
        }

        #endregion

        #region Shipping


        public virtual void DeleteBannerZone(Banner bannerZone)
        {
            if (bannerZone == null)
                throw new ArgumentNullException("bannerZone");

            _bannerRepository.Delete(bannerZone);

            _cacheManager.RemoveByPattern(BANNERSLIDER_PATTERN_KEY);
        }

        public virtual IPagedList<Banner> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            string key = string.Format(BANNERSLIDER_ALL_KEY, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = from sbw in _bannerRepository.Table
                            orderby sbw.Id
                            select sbw;

                var records = new PagedList<Banner>(query, pageIndex, pageSize);
                return records;
            });
        }

        public virtual Banner GetById(int bannerZoneId)
        {
            if (bannerZoneId == 0)
                return null;

            var record = _bannerRepository.GetById(bannerZoneId);
            return record;
        }

        public virtual Banner GetByZone(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            var zone = _bannerRepository.Table.FirstOrDefault(bs => bs.ZoneName == name);
            return zone;
        }

        public virtual void InsertBannerZone(Banner bannerZone)
        {
            if (bannerZone == null)
                throw new ArgumentNullException("bannerZone");

            _bannerRepository.Insert(bannerZone);

            _cacheManager.RemoveByPattern(BANNERSLIDER_PATTERN_KEY);
        }

        public virtual void UpdateBannerZone(Banner bannerZone)
        {
            if (bannerZone == null)
                throw new ArgumentNullException("bannerZone");

            _bannerRepository.Update(bannerZone);

            _cacheManager.RemoveByPattern(BANNERSLIDER_PATTERN_KEY);
        }

        #endregion

        #region SliderItem

        public void InsertItem(BannerItem entity)
        {
            if (entity == null)
                throw new ArgumentNullException("bannerItem is null");

            _bannerItemRepository.Insert(entity);
        }
        public BannerItem GetBannerItemById(int id)
        {
            return _bannerItemRepository.GetById(id);
        }

        public IList<BannerItem> GetItemsById(int id)
        {
            var entity = _bannerItemRepository.Table
                 .Where(x => x.BannerId == id);

            return entity.ToList();
        }

        public IList<BannerItem> GetItemsByZone(string zone)
        {
            var entity = _bannerRepository.Table
                 .Where(x => x.ZoneName == zone).FirstOrDefault();

            if (entity != null)
                return entity.Items.ToList();

            return new List<BannerItem>();
        }
        public void UpdateItem(BannerItem entity)
        {
            if (entity == null)
                throw new ArgumentNullException("bannerItem is null");

            _bannerItemRepository.Update(entity);
        }
        public void DeleteItem(BannerItem entity)
        {
            if (entity == null)
                throw new ArgumentNullException("bannerItem is null");

            _bannerItemRepository.Delete(entity);
        }

        #endregion


    }
}
