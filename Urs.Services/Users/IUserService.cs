using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Users;

namespace Urs.Services.Users
{
    public partial interface IUserService
    {
        #region Users

        IPagedList<User> GetAllUsers(DateTime? registrationFrom = null,
           DateTime? registrationTo = null, int[] userRoleIds = null, string email = null, string nickname = null,
           string company = null, string phone = null, bool? active = null, bool? approved = null, bool loadOnlyWithShoppingCart = false, int pageIndex = 0, int pageSize = int.MaxValue
            , string provinceName = null, string cityName = null, string areaName = null);

        User GetUsersByUsernameOrPhoneOrEmail(string usernameOrPhoneOrEmail);
        void DeleteUser(User user);

        User GetUserById(int userId);

        IList<User> GetUsersByIds(int[] userIds);

        User GetUserByGuid(Guid userGuid);

        User GetUserByEmail(string email);

        User GetUserByPhone(string phone);
        User GetUserByCode(string code);

        User InsertGuestUser();

        void InsertUser(User user);

        void UpdateUser(User user);

        int DeleteGuestUsers(DateTime? createdToUtc);

        #endregion

        #region User roles

        void DeleteUserRole(UserRole userRole);

        UserRole GetUserRoleById(int userRoleId);
        UserRole GetUserRoleBySystemName(string systemName);

        IList<UserRole> GetAllUserRoles(bool showHidden = false);

        void InsertUserRole(UserRole userRole);

        void UpdateUserRole(UserRole userRole);

        #endregion

        #region PointsHistory

        IPagedList<PointsHistory> GetPointsHistory(int userId, DateTime? startTime, DateTime? endTime, int pageIndex, int pageSize);

        #endregion

        #region ������
        IPagedList<User> GetInvitations(int affiliateId, int pageIndex, int pageSize);

        IList<User> GetInvitation(int userId, UserRoleType type);

        int GetInvitedCount(int userId);

        #endregion

        #region ��������ż�¼
        void InsertMessageRecord(MessageRecord messageRecord);
        void UpdateMessageRecord(MessageRecord messageRecord);
        void DeleteMessageRecord(MessageRecord messageRecord);
        MessageRecord GetMessageRecord(int userId, string mark = null, DateTime? startTime = null, DateTime? endTime = null);
        MessageRecord GetMessageRecord(string phone, DateTime startTime);
        MessageRecord GetMessageRecordById(int messagerecordId);

        IList<MessageRecord> GetMessageRecordByUserId(int userId);

        IPagedList<MessageRecord> GetMessageRecordByUserId(int userId, int pageIndex, int pageSize);

        IPagedList<MessageRecord> GetMessageRecords(int? userId, string phone, DateTime? startTime, DateTime? endtime, int pageIndex, int pageSize);
        #endregion

    }
}