﻿using Urs.Data.Domain.Users;
using System;

namespace Urs.Services.Users
{
    public class ChangePasswordRequest
    {
        public string AccountName { get; set; }

        public  Guid UserGuid { get; set; }

        public bool ValidateRequest { get; set; }
        public PasswordFormat NewPasswordFormat { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }

        public ChangePasswordRequest(Guid guid, bool validateRequest, 
            PasswordFormat newPasswordFormat, string newPassword, string oldPassword = "")
        {
            this.UserGuid = guid;
            this.ValidateRequest = validateRequest;
            this.NewPasswordFormat = newPasswordFormat;
            this.NewPassword = newPassword;
            this.OldPassword = oldPassword;
        }
    }
}
