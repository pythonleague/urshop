﻿namespace Urs.Services.Agents
{
    public partial class AgentUserSummary
    {
        public int NumberOfAgent { get; set; }
        public int NumberOfOrder { get; set; }
        public int NumberOfValidOrder { get; set; }
        public decimal AmountOfOrder { get; set; }
        public decimal AmountOfValidOrder { get; set; }
        public decimal TotalAmountOfFee { get; set; }
        public decimal AmountOfValidFee { get; set; }
        public decimal AmountOfNoValidFee { get; set; }
        public decimal CashFee { get; set; }
        public decimal NoCashFee { get; set; }
    }
}
