
using System;
using System.IO;
using System.Linq;
using System.Net;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Urs.Services.Users;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Services.Media;
using Urs.Services.Orders;

namespace Urs.Services.Authentication.External
{
    public partial class ExternalAuthorizer : IExternalAuthorizer
    {
        #region Fields

        private readonly IAuthenticationService _authenticationService;
        private readonly IOpenAuthenticationService _openAuthenticationService;
        private readonly IUserRegistrationService _userRegistrationService;
        private readonly IActivityLogService _activityLogService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly UserSettings _userSettings;
        private readonly ExternalAuthSettings _externalAuthenticationSettings;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILogger _logger;
        private readonly IUserService _userService;
        private readonly IPictureService _pictureService;
        #endregion

        #region Ctor

        public ExternalAuthorizer(IAuthenticationService authenticationService,
            IOpenAuthenticationService openAuthenticationService,
            IUserRegistrationService userRegistrationService,
            IActivityLogService activityLogService, ILocalizationService localizationService,
            IWorkContext workContext, UserSettings userSettings,
            ExternalAuthSettings externalAuthenticationSettings,
            IShoppingCartService shoppingCartService,
            ILogger logger,
            IUserService userService,
            IPictureService pictureService)
        {
            this._authenticationService = authenticationService;
            this._openAuthenticationService = openAuthenticationService;
            this._userRegistrationService = userRegistrationService;
            this._activityLogService = activityLogService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._userSettings = userSettings;
            this._externalAuthenticationSettings = externalAuthenticationSettings;
            this._shoppingCartService = shoppingCartService;
            this._logger = logger;
            this._userService = userService;
            this._pictureService = pictureService;
        }

        #endregion

        #region Utilities

        public int AvatarPicture(string imageUrl)
        {
            int pictureId = 0;
            HttpWebRequest request = WebRequest.Create(imageUrl) as HttpWebRequest;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                try
                {
                    Stream stream = response.GetResponseStream();
                    BinaryReader reader = new BinaryReader(stream);
                    byte[] bytes;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[4096];
                        int count;
                        while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            ms.Write(buffer, 0, count);
                        }
                        bytes = ms.ToArray();
                    }
                    var picture = _pictureService.InsertPicture(bytes, "image/jpeg", string.Empty, false);
                    if (picture != null)
                    {
                        pictureId = picture.Id;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message, ex);
                }
            }
            return pictureId;
        }
        #endregion

        #region Methods

        public virtual AuthorizationResult Authorize(OpenAuthParameters parameters, User userLoggedIn)
        {
            var userFound = _openAuthenticationService.GetUser(parameters);

            if (userFound == null && userLoggedIn != null)
            {
                var details = new RegistrationDetails(parameters);
                _openAuthenticationService.AssociateExternalAccountWithUser(userLoggedIn, parameters);
                var record = userLoggedIn.ExternalAuthRecords.Where(q => q.ProviderSystemName == parameters.ProviderSystemName).FirstOrDefault();
                if (record != null)
                {
                    record.Name = details.Nickname;
                }
                if (userLoggedIn.AvatarPictureId == 0 && !string.IsNullOrEmpty(details.ImageUrl))
                {
                    userLoggedIn.AvatarPictureId = AvatarPicture(details.ImageUrl);
                }
                _userService.UpdateUser(userLoggedIn);
                _activityLogService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"),
                    userFound ?? userLoggedIn);
            }
            if (userFound != null)
            {
                _shoppingCartService.MigrateShoppingCart(userLoggedIn, userFound ?? userLoggedIn);
                _authenticationService.SignIn(userFound ?? userLoggedIn, false);

                return new AuthorizationResult(OpenAuthenticationStatus.Authenticated);
            }
            if (userFound == null && userLoggedIn == null)
            {
                return new AuthorizationResult(OpenAuthenticationStatus.Authenticated);
            }
            return new AuthorizationResult(OpenAuthenticationStatus.Authenticated);
        }

        public virtual AuthorizationResult ApiAuthorize(OpenAuthParameters parameters, User userLoggedIn)
        {
            var authStatus = new AuthorizationResult(OpenAuthenticationStatus.Authenticated);
            var userFound = _openAuthenticationService.GetUser(parameters);

            if (userFound == null && userLoggedIn != null)
            {
                var details = new RegistrationDetails(parameters);
                _openAuthenticationService.AssociateExternalAccountWithUser(userLoggedIn, parameters);
                var record = userLoggedIn.ExternalAuthRecords.Where(q => q.ProviderSystemName == parameters.ProviderSystemName).FirstOrDefault();
                if (record != null)
                {
                    record.Name = details.Nickname;
                }
                if (userLoggedIn.AvatarPictureId == 0 && !string.IsNullOrEmpty(details.ImageUrl))
                {
                    userLoggedIn.AvatarPictureId = AvatarPicture(details.ImageUrl);
                }
                _userService.UpdateUser(userLoggedIn);

                _userRegistrationService.RegisterUser(request: new UserRegistrationRequest()
                {
                    User = userLoggedIn,
                    IsApproved = true,
                    Nickname = record.Name,
                    PasswordFormat = PasswordFormat.Clear,
                    UserRegistrationType = UserRegistrationType.AutoToken,
                });

                _activityLogService.InsertActivity("PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"), userLoggedIn);

                var token = _authenticationService.JwtSignIn(userLoggedIn, _userSettings.JwtSecurityKey, _userSettings.JwtExpires, _userSettings.JwtIssuer, _userSettings.JwtAudience);

                authStatus.Token = token;
                authStatus.user = userLoggedIn;
                return authStatus;
            }
            if (userFound != null)
            {
                var details = new RegistrationDetails(parameters);
                if (string.IsNullOrEmpty(userLoggedIn.Nickname))
                {
                    userLoggedIn.Nickname = details.Nickname;
                    _userService.UpdateUser(userLoggedIn);
                }

                if (userLoggedIn.AvatarPictureId == 0 && !string.IsNullOrEmpty(details.ImageUrl))
                {
                    userLoggedIn.AvatarPictureId = AvatarPicture(details.ImageUrl);
                    _userService.UpdateUser(userLoggedIn);
                }
                _shoppingCartService.MigrateShoppingCart(userLoggedIn, userFound);
                var token = _authenticationService.JwtSignIn(userFound, _userSettings.JwtSecurityKey, _userSettings.JwtExpires, _userSettings.JwtIssuer, _userSettings.JwtAudience);

                authStatus.Token = token;
                authStatus.user = userFound;
                return authStatus;
            }
            if (userFound == null && userLoggedIn == null)
            {
                return new AuthorizationResult(OpenAuthenticationStatus.Unknown);
            }
            return new AuthorizationResult(OpenAuthenticationStatus.Unknown);
        }

        #endregion
    }
}