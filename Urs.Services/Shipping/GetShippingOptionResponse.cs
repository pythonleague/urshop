using System.Collections.Generic;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Shipping
{
    public partial class GetShippingOptionResponse
    {
        public GetShippingOptionResponse()
        {
            this.Errors = new List<string>();
            this.ShippingOptions = new List<ShippingOption>();
        }

        public IList<ShippingOption> ShippingOptions { get; set; }

        public IList<string> Errors { get; set; }

        public bool Success { get; set; }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }
    }
}
