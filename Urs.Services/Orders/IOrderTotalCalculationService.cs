using System.Collections.Generic;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Orders
{
    public partial interface IOrderTotalCalculationService
    {
        decimal GetShoppingCartSubTotal(IList<ShoppingCartItem> cart);

        decimal AdjustShippingRate(decimal shippingRate, IList<ShoppingCartItem> cart);
        
        decimal GetShoppingCartAdditionalShippingCharge(IList<ShoppingCartItem> cart);

        bool IsFreeShipping(IList<ShoppingCartItem> cart);

        decimal GetShoppingCartShippingTotal(IList<ShoppingCartItem> cart, User user);
        
        decimal GetShoppingCartTotal(IList<ShoppingCartItem> cart,User user=null);
        decimal RedeemedPointsAmount(User user, decimal orderTotal, out int points);
        decimal ConvertRewardPointsToAmount(int rewardPoints);

        int ConvertAmountToRewardPoints(decimal amount);

    }
}
