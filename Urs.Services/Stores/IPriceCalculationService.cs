using System.Collections.Generic;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;

using Urs.Data.Domain.Orders;

namespace Urs.Services.Stores
{
    public partial interface IPriceCalculationService
    {
        decimal? GetSpecialPrice(Goods goods);

        decimal GetFinalPrice(Goods goods);

        decimal GetFinalPrice(Goods goods, User user, decimal attributesPrice, int quantity);

        decimal GetSubTotal(ShoppingCartItem shoppingCartItem);

        decimal GetUnitPrice(ShoppingCartItem shoppingCartItem);
    }
}
