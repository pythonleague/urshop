﻿using System;
using Urs.Core;

namespace Urs.Data.Domain.Common
{
    public class Address : BaseEntity, ICloneable
    {
        /// <summary>
        /// 电话号码
        /// </summary>
        public virtual string PhoneNumber { get; set; }
        public virtual string Name { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Company { get; set; }
        public virtual string ProvinceName { get; set; }
        public virtual string CityName { get; set; }
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 地址1
        /// </summary>
        public virtual string Address1 { get; set; }

        /// <summary>
        /// 地址2
        /// </summary>
        public virtual string Address2 { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public virtual string ZipPostalCode { get; set; }

        /// <summary>
        /// 传真
        /// </summary>
        public virtual string FaxNumber { get; set; }

        public virtual string CustomAttributes { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public virtual DateTime CreateTime { get; set; }

        public object Clone()
        {
            var addr = new Address()
            {
                Name = this.Name,
                Company = this.Company,
                ProvinceName = this.ProvinceName,
                CityName = this.CityName,
                AreaName = this.AreaName,
                Address1 = this.Address1,
                Address2 = this.Address2,
                ZipPostalCode = this.ZipPostalCode,
                PhoneNumber = this.PhoneNumber,
                FaxNumber = this.FaxNumber,
                CreateTime = this.CreateTime,
            };
            return addr;
        }
    }
}
