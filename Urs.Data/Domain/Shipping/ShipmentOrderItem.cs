using Urs.Core;

namespace Urs.Data.Domain.Shipping
{
    public partial class ShipmentOrderItem : BaseEntity
    {
        public virtual int ShipmentId { get; set; }
        /// <summary>
        /// 订单子项
        /// </summary>
        public virtual int OrderItemId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual int Quantity { get; set; }
        public virtual Shipment Shipment { get; set; }
    }
}