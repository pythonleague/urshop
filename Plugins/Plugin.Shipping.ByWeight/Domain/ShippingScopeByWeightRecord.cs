﻿using Urs.Core;

namespace Urs.Plugin.Shipping.ByWeight.Domain
{
    public partial class ShippingScopeByWeightRecord : BaseEntity
    {
        public virtual int ShippingByWeightRecordId { get; set; }

        public virtual string ProvinceCode { get; set; }

        public virtual string CityCode { get; set; }
    }
}
