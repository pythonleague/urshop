﻿using Urs.Core.Configuration;

namespace Urs.Plugin.Shipping.ByWeight
{
    /// <summary>
    /// Represents settings of the "by weight" shipping plugin
    /// </summary>
    public class ShippingByWeightSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether to limit shipping methods to configured ones
        /// </summary>
        public bool LimitMethodsToCreated { get; set; }
    }
}