﻿using System;

namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 用户登陆信息
    /// </summary>
    public partial class MoLoginInfo
    {
        /// <summary>
        /// 用户标记(Guid)
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 密钥
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime Expires { get; set; }
    }
}