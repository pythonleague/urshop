﻿using FluentValidation.Attributes;
using Plugin.Api.Validators.User;

namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 登录数据
    /// </summary>
    [Validator(typeof(MoLoginValidator))]
    public partial class MoLogin
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

    }
}