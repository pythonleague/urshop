﻿using System;
using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.UserAddress;
using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 用户订单
    /// </summary>
    public partial class MoOrderList
    {
        public MoOrderList()
        {
            Items = new List<MoOrderDetail>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 订单项
        /// </summary>
        public IList<MoOrderDetail> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

        #region Nested classes

        public partial class MoOrderDetail
        {
            public MoOrderDetail()
            {
                Items = new List<MoOrderGoods>();
                Address = new MoAddress();
            }
            /// <summary>
            /// 订单Id
            /// </summary>
            public int OrderId { get; set; }
            /// <summary>
            /// 订单Code
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// 订单Guid
            /// </summary>
            public string OrderGuid { get; set; }
            /// <summary>
            /// 支付方式
            /// </summary>
            public string PaymentMethod { get; set; }
            /// <summary>
            /// 订单金额
            /// </summary>
            public string OrderTotal { get; set; }
            /// <summary>
            /// 是否允许退换货
            /// </summary>
            public bool IsReturn { get; set; }
            /// <summary>
            /// 是否允许完成完成
            /// </summary>
            public bool IsComplete { get; set; }
            /// <summary>
            /// 是否允许取消
            /// </summary>
            public bool IsCancel { get; set; }
            /// <summary>
            /// 是否允许删除
            /// </summary>
            public bool IsDelete { get; set; }
            /// <summary>
            /// 是否允许评论
            /// </summary>
            public bool IsReview { get; set; }
            /// <summary>
            /// 是否允许再支付
            /// </summary>
            public bool IsPending { get; set; }
            /// <summary>
            /// 订单状态
            /// </summary>
            public string OrderStatus { get; set; }
            /// <summary>
            /// 订单状态Id
            /// </summary>
            public int OrderStatusId { get; set; }
            /// <summary>
            /// 备注
            /// </summary>
            public string Remark { get; set; }
            /// <summary>
            /// 订单创建时间
            /// </summary>
            public DateTime CreatedTime { get; set; }
            /// <summary>
            /// 订单商品
            /// </summary>
            public IList<MoOrderGoods> Items { get; set; }
            /// <summary>
            /// 地址
            /// </summary>
            public MoAddress Address { get; set; }

            public partial class MoOrderGoods
            {
                /// <summary>
                /// 商品Id
                /// </summary>
                public int GoodsId { get; set; }
                /// <summary>
                /// 商品名称
                /// </summary>
                public string Name { get; set; }
                /// <summary>
                /// 商品Sku
                /// </summary>
                public string Sku { get; set; }
                /// <summary>
                /// 商品url
                /// </summary>
                public string SeName { get; set; }
                /// <summary>
                /// 商品属性
                /// </summary>
                public string AttributesXml { get; set; }
                /// <summary>
                /// 商品图片
                /// </summary>
                public MoPicture Picture { get; set; }
                /// <summary>
                /// 购买数量
                /// </summary>
                public int Qty { get; set; }
                /// <summary>
                /// 商品单价
                /// </summary>
                public string UnitPrice { get; set; }
            }
        }
        #endregion
    }
}