﻿using System.Collections.Generic;
using Urs.Framework.Mvc;
using Plugin.Api.Models.Media;
using Plugin.Api.Models.Goods;
using Plugin.Api.Models.Common;
using Urs.Data.Domain.Common;

namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 用户
    /// </summary>
    public partial class MoUserSearch
    {
        public MoUserSearch()
        {
            Items = new List<MoGoodsOverview>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 商品
        /// </summary>
        public IList<MoGoodsOverview> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

    }
}