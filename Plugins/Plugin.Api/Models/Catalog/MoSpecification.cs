﻿namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 商品规格
    /// </summary>
    public partial class MoSpecification
    {
        /// <summary>
        /// 规格名称
        /// </summary>
        public string Name { get; set; }
       /// <summary>
       /// 规格
       /// </summary>
        public string Value { get; set; }
    }
}