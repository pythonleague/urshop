﻿using System.Collections.Generic;
using Urs.Framework.Mvc;
using Plugin.Api.Models.Media;
using Plugin.Api.Models.Catalog;
using Plugin.Api.Models.Goods;
using Plugin.Api.Models.Common;

namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 商品分类
    /// </summary>
    public partial class MoCategory
    {
        public MoCategory()
        {
            Picture = new MoPicture();
            Items = new List<MoGoodsOverview>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 分类url
        /// </summary>
        public string SeName { get; set; }
        /// <summary>
        /// 分类图片
        /// </summary>
        public MoPicture Picture { get; set; }
        /// <summary>
        /// 商品数组
        /// </summary>
        public IList<MoGoodsOverview> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
    }
}