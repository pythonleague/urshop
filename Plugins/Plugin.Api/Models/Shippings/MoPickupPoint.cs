﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plugin.Api.Models.Shippings
{
    /// <summary>
    /// 自提信息
    /// </summary>
    public class MoPickupPoint
    {
        /// <summary>
        /// 自提点Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 自提点名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string ProvinceName { get; set; }
        /// <summary>
        /// 市区
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 县区
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public decimal Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public decimal Longitude { get; set; }
        /// <summary>
        /// 配送费
        /// </summary>
        public decimal PickupFee { get; set; }
        /// <summary>
        /// 营业时间
        /// </summary>
        public string OpeningHours { get; set; }
    }
}
