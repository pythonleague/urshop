﻿namespace ExternalAuth.WeixinOpen.Models
{
    public class MoPhone
    {
        public string guid { get; set; }
        public string code { get; set; }
        public string encryptedData { get; set; }
        public string iv { get; set; }
    }
}
