﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Plugin.Payments.WeixinOpen.Models
{
    public class ConfigurationModel : BaseModel
    {
        [UrsDisplayName("Plugins.Payments.WeixinOpen.AppId")]
        public string AppId { get; set; }

        [UrsDisplayName("Plugins.Payments.WeixinOpen.AppKey")]
        public string AppKey { get; set; }

        [UrsDisplayName("Plugins.Payments.WeixinOpen.Partner")]
        public string Partner { get; set; }

        [UrsDisplayName("Plugins.Payments.WeixinOpen.AppSecret")]
        public string AppSecret { get; set; }
    }
}