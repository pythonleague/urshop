// pages/idcar/idcar.js
import api from '../../api/api'
import {
  uploadpicture
} from '../../api/conf'
import {
  customer
} from '../../api/conf'
import {
  getcustomer
} from '../../api/conf'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    idcarzmimg: '',
    idcarfmimg: '',
    myInfo: '',
    idcar: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCustomer()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },
  idcarZm: function() {
    var that = this
    wx.chooseImage({
      success: res => {
        wx.getFileSystemManager().readFile({
          filePath: res.tempFilePaths[0],
          encoding: 'base64',
          success: res => {}
        })
        let base64 = wx.getFileSystemManager().readFileSync(res.tempFilePaths[0], 'base64')
        wx.showLoading({
          title: '上传中',
        })
        api.post(uploadpicture, {
          base64Str: base64
        }).then(res => {
          wx.hideLoading()
          let imgUrl = res.Data.BigUrl
          this.setData({
            idcarzmimg: imgUrl
          })
        })
      }
    })
  },
  idcarFm: function() {
    var that = this
    wx.chooseImage({
      success: res => {
        wx.getFileSystemManager().readFile({
          filePath: res.tempFilePaths[0],
          encoding: 'base64',
          success: res => {}
        })
        let base64 = wx.getFileSystemManager().readFileSync(res.tempFilePaths[0], 'base64')
        wx.showLoading({
          title: '上传中',
        })
        api.post(uploadpicture, {
          base64Str: base64
        }).then(res => {
          wx.hideLoading()
          let imgUrl = res.Data.BigUrl
          this.setData({
            idcarfmimg: imgUrl
          })
        })
      }
    })
  },
  submitCustomer: function() {
    var that = this
    var idcardReg = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
    if (that.data.idcarzmimg == '' || that.data.idcarfmimg == '') {
      wx.showToast({
        title: '请上传身份证照片',
        icon: 'none'
      })
      return
    } else if (that.data.idcar == '') {
      wx.showToast({
        title: '请输入身份证号码',
        icon: 'none'
      })
      return
    } else if (!idcardReg.test(that.data.idcar)) {
      wx.showToast({
        title: '身份证号码格式错误',
        icon: 'none'
      })
      return
    }
    api.post(customer, {
      Nickname: wx.getStorageSync('nickName'),
      Form: [{
        Key: 19,
        Value: that.data.idcarzmimg
      }, {
        Key: 20,
        Value: that.data.idcarfmimg
      }, {
        Key: 21,
        Value: that.data.idcar
      }]
    }).then(res => {
      if (res.Code == 200) {
        wx.showToast({
          title: '上传成功,后台正在审核',
          icon: 'success'
        })
      } else {
        wx.showToast({
          title: '上传失败',
          icon: 'none'
        })
      }
    })
  },
  getCustomer: function() {
    var that = this
    api.get(getcustomer).then(res => {
      that.setData({
        myInfo: res.Data,
        idcarzmimg: res.Data.Fields[0].DefaultValue,
        idcarfmimg: res.Data.Fields[1].DefaultValue,
        idcar: res.Data.Fields[2].DefaultValue
      })
    })
  },
  bindKeyInput: function(e) {
    this.setData({
      idcar: e.detail.value
    })
  }
})