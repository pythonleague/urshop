﻿using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsSpecValidator))]
    public partial class GoodsSpecModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.GoodsSpecs.Fields.Name")]

        public string Name { get; set; }

    }
}