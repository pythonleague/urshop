﻿using System;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsReviewValidator))]
    public partial class GoodsReviewModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.Goods")]
        public int GoodsId { get; set; }
        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.Goods")]
        public string GoodsName { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.User")]
        public int UserId { get; set; }

        public string UserInfo { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.Title")]
        public string Title { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.ReviewText")]
        public string ReviewText { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.Rating")]
        public int Rating { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.Fields.CreateTime")]
        public DateTime CreateTime { get; set; }
    }
}