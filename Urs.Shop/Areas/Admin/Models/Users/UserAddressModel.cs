﻿using Urs.Admin.Models.Common;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class UserAddressModel : BaseModel
    {
        public int UserId { get; set; }

        public AddressModel Address { get; set; }
    }
}