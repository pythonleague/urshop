﻿using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentListModel : BaseEntityModel
    {
        public string SearchNickname { get; set; }

        public string SearchPhone { get; set; }

        public string SearchShopName { get; set; }
    }
}