﻿using Microsoft.AspNetCore.Routing;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Shipping
{
    public partial class ShippingRateMethodModel : BaseModel, IPluginModel
    {
        [UrsDisplayName("Admin.Configuration.Shipping.Providers.Fields.FriendlyName")]

        public string FriendlyName { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Providers.Fields.SystemName")]

        public string SystemName { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Providers.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Providers.Fields.IsActive")]
        public bool IsActive { get; set; }

        [UrsDisplayName("Admin.Configuration.Shipping.Providers.Fields.Logo")]
        public string LogoUrl { get; set; }
        public string ConfigurationActionName { get; set; }
        public string ConfigurationControllerName { get; set; }
        public RouteValueDictionary ConfigurationRouteValues { get; set; }
        public string ConfigurationUrl { get; set; }
    }
}