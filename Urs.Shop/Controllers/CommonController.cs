﻿
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Core;
using Urs.Data.Domain.Configuration;
using Urs.Services.Directory;
using Urs.Services.Logging;

namespace Urs.Web.Controllers
{
    public partial class CommonController : Controller
    {
        #region Fields

        private readonly IWebHelper _webHelper;
        private readonly CommonSettings _commonSettings;
        private readonly IAreaService _areaService;
        private readonly IWorkContext _workContext;
        private readonly ILogger _logger;

        #endregion

        #region Constructors

        public CommonController(IWebHelper webHelper,
            CommonSettings commonSettings,
            IAreaService areaService,
            IWorkContext workContext,
            ILogger logger)
        {
            this._webHelper = webHelper;
            this._commonSettings = commonSettings;
            this._areaService = areaService;
            this._workContext = workContext;
            this._logger = logger;
        }

        #endregion

        #region Methods

        //page not found
        public virtual IActionResult PageNotFound()
        {
            if (_commonSettings.Log404Errors)
            {
                var statusCodeReExecuteFeature = HttpContext?.Features?.Get<IStatusCodeReExecuteFeature>();
                //TODO add locale resource
                _logger.Error($"Error 404. The requested page ({statusCodeReExecuteFeature?.OriginalPath}) was not found",
                    user: _workContext.CurrentUser);
            }

            Response.StatusCode = 404;
            Response.ContentType = "text/html";

            return View();
        }

        public IActionResult Config()
        {
            return View();
        }

        //footer

        public IActionResult JavaScriptDisabledWarning()
        {
            if (!_commonSettings.DisplayJavaScriptDisabledWarning)
                return Content("");

            return PartialView();
        }
        //store is closed
        public IActionResult StoreClosed()
        {
            return View();
        }

        public JsonResult ProvincesSelector(string selectedValue)
        {
            var list = _areaService.GetProvinces();

            return Json(new SelectList(list, selectedValue));
        }

        public JsonResult CitiesSelector(string provincename)
        {
            var list = _areaService.GetCities(provincename);
            return Json(new SelectList(list));
        }

        public JsonResult AreasSelector(string provincename, string cityname)
        {
            var list = _areaService.GetAreas(provincename, cityname)
                ;
            return Json(new SelectList(list));
        }
        #endregion
    }
}
